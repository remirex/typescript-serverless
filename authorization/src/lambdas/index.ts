import { APIGatewayEvent } from 'aws-lambda';

import { createItem, getItemByEmail } from '../common/Dynamo';
import { hooksWithBodyValidation } from '../common/Hooks';
import { create } from '../requests';
import ApiResponse from '../common/ApiResponse';
import ApiResponseError from '../common/ApiResponseError';

// POST /users
const createUserHandler = async (event: APIGatewayEvent) => {
  const data = JSON.stringify(event.body);
  try {
    // check if email already exist
    const exist = await getItemByEmail(JSON.parse(data).email);
    if (exist) return new ApiResponse({ statusCode: 400, body: { message: 'You have already signed up' } });
    // create new item
    const item = await createItem(JSON.parse(data));

    return new ApiResponse({ statusCode: 201, body: item });
  } catch (err) {
    throw new ApiResponseError({ message: err.message });
  }
};

exports.createUserHandler = hooksWithBodyValidation({ bodySchema: create })(createUserHandler);

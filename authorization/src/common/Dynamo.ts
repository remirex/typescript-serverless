import { DynamoDB } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';

import { IUser, IUserInputDTO } from '../interfaces/IUser';

const TableName = process.env.STAGE + '-' + process.env.TABLE_NAME;

const {
  IS_OFFLINE,
  CONFIG_DYNAMODB_ENDPOINT,
  AWS_KEY_ID,
  AWS_KEY_SECRET,
} = process.env;

const db = IS_OFFLINE
  ? new DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: CONFIG_DYNAMODB_ENDPOINT,
    accessKeyId: AWS_KEY_ID,
    secretAccessKey: AWS_KEY_SECRET,
  })
  : new DynamoDB.DocumentClient();

export async function createItem(userInputDTO: IUserInputDTO): Promise<{ user: IUser }> {
  const params = {
    TableName,
    Item: {
      id: uuidv4(),
      ...userInputDTO,
      createdUtc: moment().utc().toISOString(),
    },
  };

  await db.put(params).promise();

  return { user: params.Item };
}

export async function getItemByEmail(email: string): Promise<IUser> {
  const params = {
    TableName,
    IndexName: 'email-index',
    KeyConditionExpression: 'email = :email',
    ExpressionAttributeValues: {
      ':email': email,
    },
  };

  const data = await db.query(params).promise();

  return data.Items[0] as IUser;
}

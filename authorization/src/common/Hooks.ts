import {
  useHooks,
  handleUnexpectedError,
  logEvent,
  parseEvent,
} from 'lambda-hooks';

import ApiResponseError from './ApiResponseError';
import ApiResponse from './ApiResponse';

const validateEventBody = async (state) => {
  const { bodySchema } = state.config;

  if (!bodySchema) {
    throw new ApiResponseError({
      message: 'missing the required body schema',
    });
  }

  try {
    const { event } = state;

    await bodySchema.validate(event.body, { strict: true, abortEarly: false });
  } catch (error) {
    const allErrors = {};
    error.inner.forEach((item) => {
      item.errors.forEach((item2) => {
        allErrors[item.path] = [item2.charAt(0).toUpperCase() + item2.slice(1).replace(/_/g, ' ')];
      });
    });
    state.exit = true; // stop lambda function
    state.response = new ApiResponse({
      statusCode: 422,
      body: {
        name: 'Validation Error',
        message: 'Some fields are not valid',
        errors: allErrors,
      },
    });
  }

  return state;
};

export const hooksWithBodyValidation = ({ bodySchema }: { bodySchema: any }): any => useHooks({
  before: [logEvent, parseEvent, validateEventBody],
  after: [],
  onError: [handleUnexpectedError],
}, {
  bodySchema,
});

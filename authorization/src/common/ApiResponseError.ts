export interface ApiResponseErrorArgs {
  statusCode?: number;
  message?: string;
}

export const defaultApiResponseErrorArgs: ApiResponseErrorArgs = {
  statusCode: 500,
  message: 'Internal server error',
};

export default class ApiResponseError extends Error {
  statusCode: number;

  message: string;

  constructor(args: ApiResponseErrorArgs = defaultApiResponseErrorArgs) {
    super(args.message ?? defaultApiResponseErrorArgs.message);
    this.statusCode = args.statusCode ?? defaultApiResponseErrorArgs.statusCode as number;
    this.message = args.message ?? defaultApiResponseErrorArgs.message as string;
  }
}

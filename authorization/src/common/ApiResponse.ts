export interface ApiResponseArgs {
  statusCode?: number;
  body?: any;
  headers?: {
    [name: string]: string, // 'Access-Control-Allow-Origin': '*' for example !!!
  };
}

export const defaultApiResponseArgs: ApiResponseArgs = {
  statusCode: 200,
  headers: {
    'Access-Control-Allow-Origin': '*', // Required for CORS
    'Access-Control-Allow-Credentials': 'true',
  },
};

export default class ApiResponse {
  statusCode: number;

  body?: string;

  headers: {
    [name: string]: string,
  };

  constructor(args: ApiResponseArgs = defaultApiResponseArgs) {
    this.statusCode = args.statusCode ?? defaultApiResponseArgs.statusCode as number;
    this.headers = {
      ...defaultApiResponseArgs.headers,
      ...(args.headers ?? {}),
    };

    if (args.body !== undefined) {
      this.body = JSON.stringify(args.body);
    }
  }
}

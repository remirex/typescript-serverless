export interface IUser {
  id: string;
  name: string;
  email: string;
  createdUtc: string;
}

export interface IUserInputDTO {
  name: string;
  email: string;
  password: string;
  acceptTerms: boolean;
}

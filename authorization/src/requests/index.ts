import * as yup from 'yup';

export const create = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().min(8).max(20).required(),
  acceptTerms: yup.boolean().required(),
});
